using tech_test_payment_api.Data;
using tech_test_payment_api.Model;
using tech_test_payment_api.Repository.Interface;

namespace tech_test_payment_api.Repository
{
    public class ItemDeVendaRepository : IItemDeVendaRepository
    {
        private readonly InMemoryDbContext _context;

        public ItemDeVendaRepository(InMemoryDbContext context)
        {
            _context = context;
        }

        public IEnumerable<ItemDeVenda> ObterItens()
        {
            return _context.ItemDeVendas.ToList();
        }

        public ICollection<ItemDeVenda> ObterItens(int[] itens)
        {
            return _context.ItemDeVendas.Where(i => itens.Contains(i.Id)).ToList();
        }

        public void RegistrarItem(ItemDeVenda itemDeVenda)
        {
            _context.ItemDeVendas.Add(itemDeVenda);
            _context.SaveChanges();
        }
    }
}