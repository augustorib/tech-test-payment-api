using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.BusinessLayer;
using tech_test_payment_api.Data;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Model;
using tech_test_payment_api.Repository.Interface;


namespace tech_test_payment_api.Repository
{
    public class VendaRepository : IVendaRepository
    {

        private readonly InMemoryDbContext _context;
        
        private readonly VendaService _vendaService;
        

        public VendaRepository(InMemoryDbContext context, VendaService vendaService)
        {
           _context = context;
           _vendaService = vendaService;
        }

        public IEnumerable<Venda> ObterVendas()
        {
            return _context.Vendas.Include(v => v.Vendedor)
                                  .Include(v => v.ItensDeVenda)
                                  .ToList();        
        }

        public void RegistrarVenda(VendaPostDTO venda, Vendedor vendedor, IEnumerable<ItemDeVenda> itens)
        {
            var novaVenda  = new Venda
            {
                Id = venda.Id,
                VendedorId = venda.VendedorId,
                Status = "Aguardando Pagamento",
                DataVenda = DateTime.Now.ToString("dddd, dd MMMM yyyy"),
                ItensDeVenda = (ICollection<ItemDeVenda>)itens,
                Vendedor = vendedor
            };

            _context.Add(novaVenda);
            _context.SaveChanges();
        }


        public Venda ObterVendaPorId(int id)
        {
            return  _context.Vendas.Include(v => v.Vendedor)
                                   .Include(v => v.ItensDeVenda)
                                   .FirstOrDefault(v => v.Id == id);
            
        }

        public void AtualizarStatusVenda(VendaPutDTO venda)
        {
            var vendaParaAtualizarStatus =  _context.Vendas.FirstOrDefault(v => v.Id == venda.Id);

            if(_vendaService.PodeAtualizarStatus(vendaParaAtualizarStatus.Status, venda.Status))
            {
                vendaParaAtualizarStatus.Status = venda.Status;
                _context.Vendas.Update(vendaParaAtualizarStatus);
                _context.SaveChanges();
            }
        }

    }
}