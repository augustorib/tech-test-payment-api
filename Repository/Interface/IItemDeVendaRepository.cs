using tech_test_payment_api.Model;

namespace tech_test_payment_api.Repository.Interface
{
    public interface IItemDeVendaRepository
    {
        IEnumerable<ItemDeVenda> ObterItens(); 
        ICollection<ItemDeVenda> ObterItens(int[] itens); 
        void RegistrarItem(ItemDeVenda itemDeVenda);
    }
}