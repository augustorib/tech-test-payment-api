using tech_test_payment_api.DTO;
using tech_test_payment_api.Model;

namespace tech_test_payment_api.Repository.Interface
{
    public interface IVendaRepository
    {
        IEnumerable<Venda> ObterVendas();  
        void RegistrarVenda(VendaPostDTO venda, Vendedor vendedor, IEnumerable<ItemDeVenda> itens);
        Venda ObterVendaPorId(int id);
        void AtualizarStatusVenda(VendaPutDTO venda);


    }
}