using tech_test_payment_api.Model;

namespace tech_test_payment_api.Repository.Interface
{
    public interface IVendedorRepository
    {
        IEnumerable<Vendedor> ObterVendedores();  
        void RegistrarVendedor(Vendedor vendedor);
        Vendedor ObterVendedorPorId(int id);
    }
}