using tech_test_payment_api.Data;
using tech_test_payment_api.Model;
using tech_test_payment_api.Repository.Interface;

namespace tech_test_payment_api.Repository
{
    public class VendedorRepository : IVendedorRepository
    {

        private readonly InMemoryDbContext _context;

        public VendedorRepository(InMemoryDbContext context)
        {
           _context = context; 

        }

        public IEnumerable<Vendedor> ObterVendedores()
        {
            return _context.Vendedores.ToList();
        }

        public void RegistrarVendedor(Vendedor vendedor)
        {
            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();
        }

        public Vendedor ObterVendedorPorId(int id)
        {
            return _context.Vendedores.FirstOrDefault(v => v.Id == id);   
        }
    }
}
