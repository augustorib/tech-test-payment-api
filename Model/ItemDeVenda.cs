

namespace tech_test_payment_api.Model
{
    public class ItemDeVenda
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public decimal Preco { get; set; } 
    }
}