
namespace tech_test_payment_api.Model
{
    public class Venda
    {
        public int Id { get; set; }
        public int VendedorId { get; set; }
        public string Status { get; set; }
        public string DataVenda { get; set; }
        public Vendedor Vendedor { get; set;}
        public ICollection<ItemDeVenda> ItensDeVenda { get; set; }
    
    }
}