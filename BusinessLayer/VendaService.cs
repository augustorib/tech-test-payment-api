namespace tech_test_payment_api.BusinessLayer
{
    public class VendaService
    {
        public bool PodeAtualizarStatus(string statusAntigo, string novoStatus)
        {
            switch(statusAntigo)
            {
                case "Aguardando Pagamento":
                    return novoStatus == "Pagamento Aprovado" || novoStatus == "Cancelada";
                case "Pagamento Aprovado":
                    return  novoStatus == "Enviado para Transportadora" || novoStatus == "Cancelada";
                case "Enviado para Transportador.":
                    return novoStatus == "Entregue";
                default:
                    return false;
            }
        }

    }
}