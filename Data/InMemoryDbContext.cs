using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Model;


namespace tech_test_payment_api.Data
{
    public class InMemoryDbContext : DbContext
    {
        public InMemoryDbContext(DbContextOptions<InMemoryDbContext> options) : base(options)
        {

        }

        public DbSet<Venda> Vendas {get; set;}
        public DbSet<ItemDeVenda> ItemDeVendas {get; set;}
        public DbSet<Vendedor> Vendedores {get; set;}

    }
}