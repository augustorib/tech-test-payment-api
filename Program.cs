using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.BusinessLayer;
using tech_test_payment_api.Data;
using tech_test_payment_api.Repository;
using tech_test_payment_api.Repository.Interface;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

//Repository Interface
builder.Services.AddScoped<IVendaRepository, VendaRepository>();
builder.Services.AddScoped<IVendedorRepository, VendedorRepository>();
builder.Services.AddScoped<IItemDeVendaRepository, ItemDeVendaRepository>();

//Business
builder.Services.AddScoped<VendaService>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<InMemoryDbContext>(
    options => options.UseInMemoryDatabase(builder.Configuration.GetConnectionString("Payment-API"))
);


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
