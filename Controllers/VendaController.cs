using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Repository.Interface;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api-docs/[controller]")]    
    public class VendaController : ControllerBase
    {

        private readonly IVendaRepository _vendaRepository;
        private readonly IVendedorRepository _vendedorRepository;
        private readonly IItemDeVendaRepository _itemDeVendaRepository;
        
        public VendaController(IVendaRepository vendaRepository, 
                               IVendedorRepository vendedorRepository,
                               IItemDeVendaRepository itemDeVendaRepository)
        {
            
            _vendaRepository = vendaRepository;
            _vendedorRepository = vendedorRepository;
            _itemDeVendaRepository = itemDeVendaRepository;
        }
            
        [HttpGet("ObterVendas")]
        public IActionResult Get()
        {
            
            var vendas =  _vendaRepository.ObterVendas();


            if(vendas == null)
            {
                return NotFound();
            }

            return Ok(vendas);
        }
     
        [HttpGet("ObterVenda/{id}")]
        public IActionResult GetVendaPorId(int id)
        {        
            var venda =  _vendaRepository.ObterVendaPorId(id);

            if(venda == null)
            {
                return NotFound();
            }

            return Ok(venda);
        }


        [HttpPost("RegistrarVenda")]
        public IActionResult Post(VendaPostDTO venda)
        {
            var vendedor = _vendedorRepository.ObterVendedorPorId(venda.VendedorId);

            if(vendedor == null)
            {
                return BadRequest("A venda não possui um vendedor");
            }

            var itens  = _itemDeVendaRepository.ObterItens(venda.Itens);
        

            if(itens == null)
            {
                return BadRequest("A venda precisa ter pelo menos 1 item");
            }

            _vendaRepository.RegistrarVenda(venda, vendedor, itens);

            return CreatedAtAction(nameof(Get), new {id = venda.Id}, venda);

        }
        
        
        [HttpPut("AtualizarStatusVenda")]
        public IActionResult Put([FromBody] VendaPutDTO venda)
        {
             _vendaRepository.AtualizarStatusVenda(venda);

            return NoContent();
        }



    }

}