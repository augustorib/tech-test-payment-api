using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Model;
using tech_test_payment_api.Repository.Interface;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ItemDeVendaController : ControllerBase
    {
        private readonly IItemDeVendaRepository _itemDeVendaRepository;

        public ItemDeVendaController(IItemDeVendaRepository itemDeVendaRepository)
        {
            _itemDeVendaRepository = itemDeVendaRepository;
        }

        [HttpGet("ObterItens")]
        public IActionResult Get()
        {
            
            var items =  _itemDeVendaRepository.ObterItens();

            if(items == null)
            {
                return NotFound();
            }

            return Ok(items);
        }

        [HttpPost("RegistrarItens")]
        public IActionResult Post(ItemDeVenda itemDeVenda)
        { 
             _itemDeVendaRepository.RegistrarItem(itemDeVenda);

            return CreatedAtAction(nameof(Get), new {id = itemDeVenda.Id}, itemDeVenda);
        }
    }
}