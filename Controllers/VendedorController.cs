using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Model;
using tech_test_payment_api.Repository.Interface;



namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api-docs/[controller]")]
    public class VendedorController : ControllerBase
    {
        
        private readonly IVendedorRepository _vendedorRepository;

        public VendedorController(IVendedorRepository vendedorRepository)
        {
            _vendedorRepository = vendedorRepository;
        }
        
        [HttpGet("ObterVendedores")]
        public IActionResult Get()
        {
            
            var vendedores =  _vendedorRepository.ObterVendedores();

            if(vendedores == null)
            {
                return NotFound();
            }

            return Ok(vendedores);
        }

        [HttpPost("RegistrarVendedor")]
        public IActionResult Post(Vendedor vendedor)
        { 
             _vendedorRepository.RegistrarVendedor(vendedor);

            return CreatedAtAction(nameof(Get), new {id = vendedor.Id}, vendedor);
        }
    }
}