

namespace tech_test_payment_api.DTO
{
    public class VendaPutDTO
    {
    
        public int Id { get; set; }
        public string Status { get; set; }

    }
}