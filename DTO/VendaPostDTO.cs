using System.Text.Json.Serialization;
using tech_test_payment_api.Model;

namespace tech_test_payment_api.DTO
{
    public class VendaPostDTO
    {
        [JsonIgnore]
        public int Id { get; set; }
        public int VendedorId { get; set; }
        [JsonIgnore]
        public Vendedor Vendedor { get; set;}

        public int[] Itens { get; set;}
        
        [JsonIgnore]
        public ICollection<ItemDeVenda> ItensDeVenda { get; set; }
    }
}